package com.hackacthon.finance.app.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackacthon.finance.app.controller.TradeController;
import com.hackacthon.finance.app.entities.Trade;
import com.hackacthon.finance.app.service.TradeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
@ContextConfiguration(classes={com.hackacthon.finance.app.AppApplication.class})
public class TradeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService service;

    @Autowired
    private ObjectMapper mapper;

    private static String exampleTestId = "5f4d6497e3126f33ba3dffda";

    private Trade trade;

    @Before
    public void init() {
        this.trade = new Trade(exampleTestId, Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                "AMZN", 2, 2000.0);
    }

    @Test
    public void testCanRetrieveTradeById() throws Exception {

        given(service.getTradeById(exampleTestId)).willReturn(Optional.of(this.trade));

        mockMvc.perform(get("/trade/" + exampleTestId).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.ticker", is("AMZN")));
    }

    @Test
    public void testCanRetrieveAll() throws Exception {
        List<Trade> trades = new ArrayList<Trade>();
        trades.add(this.trade);

        given(service.getPortfolio()).willReturn(trades);

        mockMvc.perform(get("/trade")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].ticker", is("AMZN")));

    }

    @Test
    public void testCanAddTrade() throws Exception {
        mockMvc.perform(post("/trade")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(this.trade)))
                .andExpect(status().isOk());

        verify(service).addToPortfolio(any(Trade.class));
    }

    @Test
    public void testCanUpdateTrade() throws Exception {
        mockMvc.perform(put("/trade")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(this.trade)))
                .andExpect(status().isOk());

        verify(service).updateTrade(any(Trade.class));
    }

    @Test
    public void testDeleteTrade() throws Exception {

        mockMvc.perform(delete("/trade")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(this.trade))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(service).deleteTrade(any(Trade.class));
    }

    @Test
    public void testDeleteTradeById() throws Exception {

        mockMvc.perform(delete("/trade/{id}", this.trade.get_id())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(this.trade))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(service).deleteTrade(this.trade.get_id());
    } 

}
