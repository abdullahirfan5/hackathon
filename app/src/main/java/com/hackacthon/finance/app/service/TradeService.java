package com.hackacthon.finance.app.service;

import com.hackacthon.finance.app.entities.Trade;
import com.hackacthon.finance.app.repository.TradeRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class TradeService {

    private static final Logger LOG = LoggerFactory.getLogger(TradeService.class);

    @Autowired
    private TradeMonitoring tradeMonitoring;

    @Autowired
    private TradeRepo dao;

    private List<Trade> tradeHistory = new ArrayList<Trade>();

    public void addToPortfolio(Trade trade) {
        dao.insert(trade);
        tradeMonitoring.msgChannel(trade.toString());
        this.tradeHistory.add(trade);
    }

    public Collection<Trade> getPortfolio() {
        return dao.findAll();
    }

    public List<Trade> getTradeHistory() { return this.tradeHistory; };

    public Optional<Trade> getTradeById(String id) {
        return dao.findById(id);
    }

    public void updateTrade(Trade trade){
        dao.save(trade);
    }

    public void deleteTrade(String id) {
        dao.deleteById(id);
    }

    public void deleteTrade (Trade trade) { 
        dao.delete(trade); 
    }

}