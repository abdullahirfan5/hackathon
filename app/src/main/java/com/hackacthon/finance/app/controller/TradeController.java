package com.hackacthon.finance.app.controller;

import com.hackacthon.finance.app.entities.Trade;
import java.util.Collection;
import java.util.Optional;
import com.hackacthon.finance.app.service.TradeService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/trade")
@CrossOrigin
public class TradeController {

    @Autowired
    private TradeService service;

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Optional<Trade> getTradeById(@PathVariable("id") String id) {
        return service.getTradeById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public Collection<Trade> getPortfolio() {
        return service.getPortfolio();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addTrade(@RequestBody Trade trade) {
        service.addToPortfolio(trade);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void updateTrade(@RequestBody Trade trade) {
        service.updateTrade(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteTrade(@RequestBody Trade trade) {
        service.deleteTrade(trade);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void getDeleteById(@PathVariable("id") String id) {
        service.deleteTrade(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/history")
    public Collection<Trade> getTradeHistory() { return service.getTradeHistory(); }
}