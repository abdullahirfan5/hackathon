package com.hackacthon.finance.app.service;

import com.hubspot.slack.client.SlackClient;
import com.hubspot.slack.client.SlackClientFactory;
import com.hubspot.slack.client.SlackClientRuntimeConfig;
import com.hubspot.slack.client.methods.params.chat.ChatPostMessageParams;
import com.hubspot.slack.client.methods.params.conversations.ConversationOpenParams;
import com.hubspot.slack.client.methods.params.users.UserEmailParams;
import com.hubspot.slack.client.models.response.conversations.ConversationsOpenResponse;
import com.hubspot.slack.client.models.response.users.UsersInfoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TradeMonitoring {

    private static final String ACCESS_TOKEN = "xoxb-1358098698451-1381762599232-kYPhouHM24CfDoEsSytb01LP";
    private static final String REPORTING_CHANNEL= "tradeapp";

    private SlackClientRuntimeConfig runtimeConfig = SlackClientRuntimeConfig.builder()
            .setTokenSupplier(() -> ACCESS_TOKEN)
            .build();

    private SlackClient slackClient = SlackClientFactory.defaultFactory().build(runtimeConfig);

    public void msgChannel(String message){

        message = "====================\n" + message + "\n====================";

        slackClient.postMessage(
                ChatPostMessageParams.builder()
                        .setText(message)
                        .setChannelId(REPORTING_CHANNEL)
                        .build()
        ).join().unwrapOrElseThrow();
    }

    public void msgUser(String message, String user) {

        UsersInfoResponse usersInfoResponse = slackClient
                .lookupUserByEmail(UserEmailParams.builder()
                        .setEmail(user)
                        .build()
                ).join().unwrapOrElseThrow();

        ConversationsOpenResponse conversationsOpenResponse = slackClient.openConversation(ConversationOpenParams.builder()
                .addUsers(usersInfoResponse.getUser().getId())
                .build()
        ).join().unwrapOrElseThrow();

        slackClient.postMessage(
                ChatPostMessageParams.builder()
                        .setText(message)
                        .setChannelId(conversationsOpenResponse.getConversation().getId())
                        .build()
        ).join().unwrapOrElseThrow();

    }

}
