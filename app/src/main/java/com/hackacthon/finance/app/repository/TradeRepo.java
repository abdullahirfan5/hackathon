package com.hackacthon.finance.app.repository;


import com.hackacthon.finance.app.entities.Trade;
import com.hackacthon.finance.app.entities.TradeState;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TradeRepo extends MongoRepository <Trade, String> {

    List<Trade> findByState(TradeState state);

}