package com.hackacthon.finance.app.entities;

public enum TradeState {
    CREATED("CREATED"),
    PROCESSING("PROCESSING"),
    FILLED("FILLED"),
    REJECTED("REJECTED");


private String status;

private TradeState(String status){
    this.status = status;
}

public String getStatus(){
    return this.status;
}
}

