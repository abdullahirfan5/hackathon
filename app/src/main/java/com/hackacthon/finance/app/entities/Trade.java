package com.hackacthon.finance.app.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

    @Id
    private String _id;
    private Date date;
    private String ticker;
    private double amount;
    private double price;
    private TradeState state = TradeState.CREATED;

    public Trade() {}

    public Trade(String id, Date date, String ticker, double amount, double price) {
        this._id = id;
        this.date = date;
        this.ticker = ticker;
        this.amount = amount;
        this.price = price;
        this.state = TradeState.CREATED;
    }

    public String toString(){
        return "ID: " + this._id + "\n" +
                "Ticker: " + this.ticker + "\n" +
                "Date: " + this.date.toString() + "\n" +
                "Amount: " + this.amount + "\n" +
                "Price: " + this.price + "\n" +
                "State: " + this.state + "\n";
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public TradeState getState() {
        return state;
    }

    public void setState(TradeState state) {
        this.state = state;
    }
}