import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Input,
    OnInit,
    QueryList,
    ViewChildren,
} from '@angular/core';
import { SBSortableHeaderDirective, SortEvent } from '@modules/tables/directives';
import { Country } from '@modules/tables/models';
import { CountryService } from '@modules/tables/services';
import { Observable, of, } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { SharedService } from './../../../dashboard/services/shared.service';

@Component({
    selector: 'sb-ng-bootstrap-table',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './ng-bootstrap-table.component.html',
    styleUrls: ['ng-bootstrap-table.component.scss'],
})
export class NgBootstrapTableComponent implements OnInit {
    @Input() pageSize = 4;

    countries$!: Observable<Country[]>;
    total$!: Observable<number>;
    sortedColumn!: string;
    sortedDirection!: string;
    clickEventsubscription:Subscription;

    @ViewChildren(SBSortableHeaderDirective) headers!: QueryList<SBSortableHeaderDirective>;

    constructor(
        public countryService: CountryService,
        private changeDetectorRef: ChangeDetectorRef,
        private http: HttpClient,
        private sharedService:SharedService,
        private ref: ChangeDetectorRef
    ) {

        this.clickEventsubscription = this.sharedService.getClickEvent().subscribe(()=>{
            this.ngOnInit();
        });

    }

    ngOnInit() {
        this.countryService.pageSize = this.pageSize;
        this.countries$ = new Observable<Country[]>();
        this.total$ = this.countryService.total$;

        this.http.get<Country[]>('http://localhost:8090/trade').toPromise().then(data => {
            this.countries$ = of(data);
            this.ref.detectChanges();
        });

    }

    onDelete(id:string) {
        this.http.delete('http://localhost:8090/trade/' + id).toPromise().then(data => {  this.ref.detectChanges();  });
        this.ngOnInit();
    }

    onSort({ column, direction }: SortEvent) {
        this.sortedColumn = column;
        this.sortedDirection = direction;
        this.countryService.sortColumn = column;
        this.countryService.sortDirection = direction;
        this.changeDetectorRef.detectChanges();
    }
}
