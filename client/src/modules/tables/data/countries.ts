import { Country } from '../models';

export const COUNTRIES: Country[] = [
    
    {
        id: 1,
        name: 'NASDAQ: AMZN',
        price: 2993.98,
        quantity: 10,
    },
    {
        id: 2,
        name: 'NASDAQ: AAPL',
        price: 109.57,
        quantity: 7,
    },
    {
        id: 3,
        name: 'DOW',
        price: 27817.90,
        quantity: 4,
    },
    {
        id: 4,
        name: 'S&P 500',
        price: 3341.92,
        quantity: 20,
    },
    {
        id: 5,
        name: 'Gold',
        price: 1950.80,
        quantity: 3,
    },
    {
        id: 6,
        name: 'Oil',
        price: 41.05,
        quantity: 9,
    },
    {
        id: 7,
        name: 'Genpact Ltd.',
        price: 39.51,
        quantity: 10,
    },
    {
        id: 8,
        name: 'GOOGL',
        price: 1473.07,
        quantity: 5,
    },
    {
        id: 9,
        name: 'eBay',
        price: 48.59,
        quantity: 3,
    },
    {
        id: 10,
        name: 'IBM',
        price: 124.40,
        quantity: 15,
    },
    {
        id: 11,
        name: 'MSFT',
        price: 201.24,
        quantity: 1,
    },
    {
        id: 12,
        name: 'TSLA',
        price: 491.84,
        quantity: 6,
    },
    {
        id: 13,
        name: 'CCL',
        price: 16.11,
        quantity: 5,
    },

    {
        id: 14,
        name: 'KRX',
        price: 59500,
        quantity: 2,
    },

    {
        id: 15,
        name: 'SBUX',
        price: 85.65,
        quantity: 30,
    }
];
