export interface Country {
    [key: string]: string | number;
    id: number;
    name: string;
    price: number;
    quantity: number;
}
