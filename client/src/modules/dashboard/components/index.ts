import { DashboardCardsComponent } from './dashboard-cards/dashboard-cards.component';
import { DashboardTablesComponent } from './dashboard-tables/dashboard-tables.component';
import { DashboardTradeComponent } from './dashboard-trade/dashboard-trade.component';

export const components = [DashboardCardsComponent, DashboardTablesComponent, DashboardTradeComponent];

export * from './dashboard-cards/dashboard-cards.component';
export * from './dashboard-tables/dashboard-tables.component';
export * from './dashboard-trade/dashboard-trade.component';
