import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SharedService } from './../../services/shared.service';

@Component({
  selector: 'sb-dashboard-trade',
  templateUrl: './dashboard-trade.component.html',
  styleUrls: ['./dashboard-trade.component.scss']
})
export class DashboardTradeComponent implements OnInit {

  constructor(private http: HttpClient, private sharedService:SharedService) { }



  ngOnInit(): void {
  }
  onClickMe(ticker: string, date: Date, amount: number, price: number) {

    if(ticker.trim().length === 0) alert("Please enter a Company Ticker");
    else if(date.toString() === "") alert("Please enter a date");
    else if(amount.toString() === "") alert("Please enter an amount");
    else if(price.toString() === "") alert("Please enter a price");
    else {
      var JSONObj = { "ticker": ticker, "date": date, "amount": amount, "price": price };

      this.http.post<any>('http://localhost:8090/trade', JSONObj).subscribe(data => {      })
    
      this.sharedService.sendClickEvent();

    }


  }


}
