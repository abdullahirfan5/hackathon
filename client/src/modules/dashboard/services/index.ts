import { DashboardService } from './dashboard.service';
import { SharedService } from './shared.service';

export const services = [DashboardService, SharedService];

export * from './dashboard.service';
export * from './shared.service';
