/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { PriceModule } from './price.module';

/* Containers */
import * as priceContainers from './containers';

/* Guards */
// import * as dashboardGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'Live Price Service',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: priceContainers.PriceComponent,
    },
];

@NgModule({
    imports: [PriceModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class PriceRoutingModule {}
