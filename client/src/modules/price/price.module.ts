/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

/* Modules */
import { AppCommonModule } from '@common/app-common.module';
import { NavigationModule } from '@modules/navigation/navigation.module';
import { ChartsModule } from '@modules/charts/charts.module';
import { TablesModule } from '@modules/tables/tables.module';

/* Components */
import * as priceComponents from './components';

/* Containers */
import * as priceContainers from './containers';

/* Guards */
// import * as dashboardGuards from './guards';

/* Services */
import * as priceServices from './services';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        FormsModule,
        AppCommonModule,
        NavigationModule,
        ChartsModule,
        TablesModule,
    ],
    providers: [...priceServices.services],
    declarations: [...priceContainers.containers, ...priceComponents.components],
    exports: [...priceContainers.containers, ...priceComponents.components],
})
export class PriceModule {}
