import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-price',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './price.component.html',
    styleUrls: ['price.component.scss'],
})
export class PriceComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
