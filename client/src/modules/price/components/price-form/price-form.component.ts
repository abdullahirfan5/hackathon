import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Price } from '@modules/price/models';
import { catchError } from 'rxjs/operators';

@Component({
    selector: 'sb-price-form',
    templateUrl: './price-form.component.html',
    styleUrls: ['./price-form.component.scss'],
})
export class PriceFormComponent implements OnInit {
    constructor(private http: HttpClient, private ref: ChangeDetectorRef) {}

    price = '';
    priceUrl = 'http://localhost:5000/price/';
    ngOnInit(): void {}

    onClickMe(ticker: string) {
        this.http.get<Price>(this.priceUrl + ticker).subscribe(
            data => {
                this.price = '$' + data.price + ' (USD)';
                this.ref.detectChanges();
            },
            error => {
                this.price = 'Error';
                this.ref.detectChanges();
            }
        );
    }
}
