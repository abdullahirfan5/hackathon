import { SideNavItems, SideNavSection } from '@modules/navigation/models';

export const sideNavSections: SideNavSection[] = [
    {
        text: 'TRADING',
        items: ['dashboard'],
    },
    {
        text: 'INFO',
        items: ['layouts'],
    },
];

export const sideNavItems: SideNavItems = {
    dashboard: {
        icon: 'tachometer-alt',
        text: 'Dashboard',
        link: '/dashboard',
    },
    layouts: {
        icon: 'columns',
        text: 'Help',
        submenu: [
            {
                text: 'Live Price',
                link: '/price',
            },
            {
                text: 'Trade Advice',
                link: '/advice',
            },
        ],
    },
};
