import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
    selector: 'sb-advice',
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './advice.component.html',
    styleUrls: ['advice.component.scss'],
})
export class AdviceComponent implements OnInit {
    constructor() {}
    ngOnInit() {}
}
