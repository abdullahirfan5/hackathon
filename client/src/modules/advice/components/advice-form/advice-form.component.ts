import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Advice } from '@modules/advice/models';
import { catchError } from 'rxjs/operators';

@Component({
    selector: 'sb-advice-form',
    templateUrl: './advice-form.component.html',
    styleUrls: ['./advice-form.component.scss'],
})
export class AdviceFormComponent implements OnInit {
    constructor(private http: HttpClient, private ref: ChangeDetectorRef) {}

    advice = '';
    adviceUrl = 'http://localhost:5001/v1/advice';
    ngOnInit(): void {}

    onClickMe(ticker: string, days: string) {
        this.http.get<Advice>(this.adviceUrl + '?ticker=' + ticker + '&days=' + days).subscribe(
            data => {
                this.advice = data.advice;
                this.ref.detectChanges();
            },
            error => {
                this.advice = 'Error';
                this.ref.detectChanges();
            }
        );
    }
}
