import { AdviceFormComponent } from './advice-form/advice-form.component';

export const components = [AdviceFormComponent];

export * from './advice-form/advice-form.component';
