/* tslint:disable: ordered-imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

/* Module */
import { AdviceModule } from './advice.module';

/* Containers */
import * as adviceContainers from './containers';

/* Guards */
// import * as dashboardGuards from './guards';

/* Routes */
export const ROUTES: Routes = [
    {
        path: '',
        data: {
            title: 'Trade Advice Service',
            breadcrumbs: [
                {
                    text: 'Dashboard',
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: adviceContainers.AdviceComponent,
    },
];

@NgModule({
    imports: [AdviceModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class AdviceRoutingModule {}
