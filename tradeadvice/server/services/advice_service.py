import datetime
import logging
import os

import pandas_datareader as pdr

# Days

LOG = logging.getLogger(__name__)

def get_business_days(days):
    return int((days * 21) / 30)

def get_advice(days, ticker):
    LOG.debug('get: ' + str(ticker))

    days = int(days)
    start_date = datetime.datetime.now() - datetime.timedelta(days)
    start_date = str(start_date)

    try:
        df = pdr.get_data_yahoo(ticker, start_date)[['Close']]
    except Exception as ex:
        LOG.error('Error getting data: ' + str(ex))
        return

    LOG.debug('Received: ' + str(df.shape) + ' from yahoo')

    business_days = get_business_days(days)

    df['mavg'] = df['Close'].rolling(window=business_days).mean()
    df['std'] = df['Close'].rolling(window=business_days).std()

    df['Upper Band'] = df['mavg'] + (df['std'] * 2)
    df['Lower Band'] = df['mavg'] - (df['std'] * 2)

    response = 'Hold'

    if df['Close'].iloc[-1] > df['Upper Band'].iloc[-1]:
        response = 'Sell'

    elif df['Close'].iloc[-1] < df['Lower Band'].iloc[-1]:
        response = 'Buy'

    LOG.debug(df.tail())
    return { "advice" : response }
