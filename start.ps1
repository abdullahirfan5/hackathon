# Taken from https://dzhavat.github.io/2020/04/09/powershell-script-to-kill-a-process-on-windows.html
function Kill-Process {
    param ($port)

    $foundProcesses = netstat -ano | findstr :$port
    $activePortPattern = ":$port\s.+LISTENING\s+\d+$"
    $pidNumberPattern = "\d+$"

    if ($foundProcesses | Select-String -Pattern $activePortPattern -Quiet) {
        $matches = $foundProcesses | Select-String -Pattern $activePortPattern
        $firstMatch = $matches.Matches.Get(0).Value

        $pidNumber = [regex]::match($firstMatch, $pidNumberPattern).Value

        taskkill /pid $pidNumber /f
    }
}

function Install-Packages {
    
    # Install Chocolatey
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    choco install -y python3
    refreshenv

    # Install pip
    python -m pip install --upgrade pip
    refreshenv

    # Install packages for tradeadvice service
    Set-Location -Path ".\tradeadvice"
    pip install -r requirements.txt --ignore-installed
    Set-Location -Path "..\"

    # Install packages for live-price service
    Set-Location -Path ".\livepriceservice"
    pip install -r requirements.txt --ignore-installed
    Set-Location -Path "..\"
}



Write-Host "Installing package dependencies for Flask"
Install-Packages

Write-Host "Terminating processes running on ports: 8089, 8090, 5000, and 5001"
Kill-Process -port 8089
Kill-Process -port 8090
Kill-Process -port 5000
Kill-Process -port 5001

Write-Host "Deleting the artifacts"
if (Test-Path -Path ".\app\build" -PathType Any) {
    Remove-Item ".\app\build" -Recurse -Force
}

if (Test-Path -Path ".\dummy-trade-filler\build" -PathType Any) {
    Remove-Item ".\dummy-trade-filler\build" -Recurse -Force
}

Write-Host "Building the app with Gradle"

Set-Location -Path ".\app"
gradle wrapper
.\gradlew.bat build
Set-Location -Path "..\"

Set-Location -Path ".\dummy-trade-filler"
gradle wrapper
.\gradlew.bat build
Set-Location -Path "..\"


Write-Host "Starting the app..."

Start-Process java -ArgumentList '-DSERVER_PORT=8089', '-jar', './dummy-trade-filler/build/libs/trade-simulator-0.0.1-SNAPSHOT.jar'
Start-Process java -ArgumentList '-DSERVER_PORT=8090', '-jar', './app/build/libs/app-0.0.1-SNAPSHOT.jar'
Start-Process python -ArgumentList '.\livepriceservice\flaskpandas_liveservice.py'
Start-Process python -ArgumentList '.\tradeadvice\manage.py', 'run'

Write-Host "The app has started!"