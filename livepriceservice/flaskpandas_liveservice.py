import datetime

from flask import Flask
from flask_restplus import Resource, Api, fields
from flask_cors import CORS
import pandas_datareader as pdr

app = Flask(__name__)
api = Api(app)
CORS(app)

@api.route('/price/<string:ticker>')
class Price(Resource):
    def get(self, ticker):
        start_date = datetime.datetime.now() - datetime.timedelta(1)

        df = pdr.get_data_yahoo(ticker, start_date)[['Close']]

        print(str(df))

        df['Date'] = df.index

        return {'ticker': ticker,
                'date': df.iloc[-1]['Date'].strftime("%m/%d/%Y"),
                'price': str(round(df.iloc[-1]['Close'], 2))}


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)